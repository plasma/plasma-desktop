# Hindi translations for plasma-desktop package.
# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Kali <EMAIL@ADDRESS>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-03-09 00:42+0000\n"
"PO-Revision-Date: 2024-12-15 15:58+0530\n"
"Last-Translator: Kali <EMAIL@ADDRESS>\n"
"Language-Team: Hindi <fedora-trans-hi@redhat.com>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: KeyboardButton.qml:19
msgid "Keyboard Layout: %1"
msgstr ""

#: Login.qml:85
msgid "Username"
msgstr ""

#: Login.qml:102
msgid "Password"
msgstr ""

#: Login.qml:144 Login.qml:150
msgid "Log In"
msgstr ""

#: Main.qml:197
msgid "Caps Lock is on"
msgstr ""

#: Main.qml:209 Main.qml:348
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr ""

#: Main.qml:215 Main.qml:354
msgid "Restart"
msgstr ""

#: Main.qml:221 Main.qml:360
msgid "Shut Down"
msgstr ""

#: Main.qml:227
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr ""

#: Main.qml:334
msgid "Type in Username and Password"
msgstr ""

#: Main.qml:366
msgid "List Users"
msgstr ""

#: Main.qml:440
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr ""

#: Main.qml:507
msgid "Login Failed"
msgstr ""

#: SessionButton.qml:18
msgid "Desktop Session: %1"
msgstr ""
